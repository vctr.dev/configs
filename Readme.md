## TODO

- Move all setup into `./install.sh`

## Restore from backup instructions

```
apt install git
git clone git@gitlab.com:victor.z.chan/configs.git ~/dotfiles
cd ~/dotfiles
./install.sh
```

- Setup `.taskrc`
		- Set up the default `~/.taskrc`
    - Change directory in taskrc to point to the correct file
		- `echo "include <configs_repo>/.taskwarrior/.taskrc" >> ~/.taskrc`
- Setup `.ssh/config`
		- `cat <configs_repo>/.ssh/config >> ~/.ssh/config`
- Install things required for Accelo
    - Set up ssh key
    - Clone repo
    - `cpanm perlcritic`
    - `cpanm perltidy`
    - In `~/.bashrc`, add `export PERL5LIB="<Accelo_repo_path>/t/lib/critic_policies"`
    - `cd <Accelo_repo_path> && git config user.name 'Victor Chan' && git config user.email 'victor.chan@accelo.com'`
    - Follow vbox instructions on wiki
		- `echo "set wildignore+=node_modules/**/*,bower_modules/**/*,**/accelo.bundle.js,**/accelo.bundle.min.js" >> <Accelo_repo_path>/../.vimrc`

## Optionals

- Wallpapers: `~/Pictures/wallpaper/*.jpg`
- Lockscreens: `~/Pictures/lock/*.png`
- Ergodox layout: `https://configure.ergodox-ez.com/keyboard_layouts/vldnoy/edit`
- Mac stuff: `stow mac`
- Brightness: `SUBSYSTEM=="backlight", ACTION=="add", RUN+="/bin/chmod a+w /sys/class/backlight/amdgpu_bl0/brightness"` into `/etc/udev/rules.d/30-brightness.rules`
- Touchpad: Use `touchpad-indicator` to disable touchpad on typing

## Network

ip addr
ip link show <dev name>
ip link set <dev name> up

## Test

- Bash should use vim mode, magic space and c-l to clear screen
- Media keys (brightness, play-pause, forward, backwards, vol, printscreen) should work in i3wm
- Vim should load with dracula theme
- Notifications should appear with the same theme as i3statusbar
- Blueman applet should appear on i3statusbar
- Music and current active taskwarrior task should appear on i3statusbar
