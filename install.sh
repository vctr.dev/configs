#!/usr/bin/env sh

add-apt-repository ppa:jonathonf/i3
add-apt-repository ppa:atareao/atareao

apt update

# -- useful tools --
apt install arandr # To manage displays
apt install wget curl # To download files
apt install stow # To manage config
apt install htop
apt install taskwarrior tasksh
apt install thefuck
apt install touchpad-indicator
apt install xautolock
apt install nomacs #Image Viewer
apt install silversearcher-ag

# -- Terminal setup --

apt install rxvt-unicode
stow Xresources

# -- File browser --

apt install ranger w3m
stow ranger

# -- bash setup --

stow bash

# -- Language Stuff --
apt install python3-dev nodejs
apt install npm
apt install cpanminus
apt install libxml2-utils pylint3

npm install -g yarn
yarn global add eslint
yarn global add markdownlint

stow markdownlint

cpanm JSON

# -- fonts --
apt install fonts-hack # Nice font

wget https://github.com/FortAwesome/Font-Awesome/releases/download/5.10.1/fontawesome-free-5.10.1-desktop.zip
unzip fontawesome-free-5.10.1-desktop.zip -d ~/fonts/
rm fontawesome-free-5.10.1-desktop.zip

# -- builtin --

stow input

# -- vim --
apt install vim-gtk

# For .vim/bundle/YouCompleteMe/install.py
apt install build-essential cmake

stow vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
python3 ~/.vim/bundle/YouCompleteMe/install.py --ts-completer

# -- i3 --
apt install \
	i3-gaps j4-dmenu-desktop \
	compton feh dunst \
	brightnessctl flameshot\
	blueman

wget https://github.com/acrisci/playerctl/releases/download/v2.0.2/playerctl-2.0.2_amd64.deb
sudo dpkg -i playerctl-2.0.2_amd64.deb
rm playerctl-2.0.2_amd64.deb

stow i3
stow i3statusbar
stow compton

# -- git --
stow git
yarn global add diff-so-fancy
