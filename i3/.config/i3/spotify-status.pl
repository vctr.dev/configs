use strict;

use constant {
    PLAY_SYMBOL  => "\x{f04b}", # 
    PAUSE_SYMBOL => "\x{f04c}", # ,
};

sub get_spotify_status {
    my $string = _format_message( _get_status(), _get_long_song_details() );
    return $string;
}

sub _format_message {
    my ( $status, $song) = @_;
	if ($status eq 'Playing') {
		return PLAY_SYMBOL . "  $song";
	} elsif ($status eq 'Paused') {
		return PAUSE_SYMBOL . "  $song";
	}
	return '';
}

sub _get_status {
    return `playerctl status 2>/dev/null | tr -d "\\n"`;
}

sub _get_long_song_details {
	return `playerctl metadata --format "{{ title }} - {{ album }} - {{ artist }}" | tr -d "\\n"`;
}

1;
