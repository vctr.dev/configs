#!/usr/bin/perl
# © 2012 Michael Stapelberg, Public Domain

# This script is a simple wrapper which prefixes each i3status line with custom
# information. To use it, ensure your ~/.i3status.conf contains this line:
#     output_format = "i3bar"
# in the 'general' section.
# Then, in your ~/.i3/config, use:
#     status_command i3status | ~/i3status/contrib/wrapper.pl
# In the 'bar' section.

use strict;
use warnings;
# You can install the JSON module with 'cpan JSON' or by using your
# distribution’s package management system, for example apt-get install
# libjson-perl on Debian/Ubuntu.
use JSON;

# Don’t buffer any output.
$| = 1;

# Skip the first line which contains the version header.
print scalar <STDIN>;

# The second line contains the start of the infinite array.
print scalar <STDIN>;

require './.config/i3/spotify-status.pl';

# Read lines forever, ignore a comma at the beginning if it exists.
while (my ($statusline) = (<STDIN> =~ /^,?(.*)/)) {
    # Decode the JSON-encoded line.
    my @blocks = @{decode_json($statusline)};

    # Prefix our own information (you could also suffix or insert in the
    # middle).
    my $spotify_status = get_spotify_status();
    unshift @blocks, { full_text => $spotify_status } if $spotify_status;

	# my $active_tasks = get_active_tasks();
	# unshift @blocks, { full_text => "\x{f00c} $active_tasks" } if $active_tasks;

    # Output the line as JSON.
    print encode_json(\@blocks) . ",\n";
}

sub get_active_tasks {
    my $started_tasks = `task +ACTIVE +PENDING _zshids | head -n 1 | tr -d '\\n'`;

    return $started_tasks;
}
