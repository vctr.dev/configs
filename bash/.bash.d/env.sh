export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad
export PATH=$PATH:$HOME/bin:$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin
export EDITOR=/usr/bin/vim
# export PS1='\[\033[01;34m\]\w\[\033[00m\]: '
