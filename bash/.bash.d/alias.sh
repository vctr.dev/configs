alias g='git'
alias codetipios='codetip -r "*.{h,m,pch,swift}"'
alias gitPushRequest='open `git push | sed -n "s/^.*\(http.*\).*$/\1/p"`'
alias gitRemoteBranchInfo='for branch in `git branch -r | grep -v HEAD`;do echo -e `git show --format="%ai %ar by %an" $branch | head -n 1` \\t$branch; done | sort -r'
alias gitRemoveMerged='git branch --merged | grep -v "\*" | grep -v master | grep -v dev | xargs -n 1 git branch -d'
alias jekyll='bundle exec jekyll serve'
alias karmaDots='karma start --browsers PhantomJS --config karma.conf.js --reporters dots'
alias ls='ls -GhF --color=auto'
alias vboxOn='VBoxManage startvm "Accelo Local Development" --type headless'
alias gs='git status'
alias node_dep='bash bin/install_node_dependencies.sh'
alias firefox-personal='firefox -P Personal'
alias firefox-work='firefox -P Work'
# alias screen='screen -a'
alias mkdir='mkdir -p'

# Taskwarrior shortcuts
alias t='task'
alias in='task add +inbox'
alias tw='task add +work'

# Bash
alias reload='source ~/.bashrc'

# https://github.com/nvbn/thefuck
eval $(thefuck --alias)

alias kurt='curl -s https://gist.githubusercontent.com/victorzchan/18aacda43c28499c18f5117d5d916965/raw/e4bfad666060ebb8cf2af3cf8838bbf1a9b4e087/kurt | less'
alias runGitBackup='git commit -am "Daily Log" && git push'

# Notifications
alias pause-dunst='killall -SIGUSR1 dunst' # pause
alias resume-dunst='killall -SIGUSR2 dunst' # resume

# From https://faq.i3wm.org/question/3747/enabling-multimedia-keys/?answer=3759#post-id-3759
# Pulse Audio controls
alias vup='pactl set-sink-volume 0 +5%' #increase sound volume
alias vdown='pactl set-sink-volume 0 -5%' #decrease sound volume
alias vmute='pactl set-sink-mute 0 toggle' # mute sound

# Media player controls
alias mplay='playerctl play'
alias mpause='playerctl pause'
alias mnext='playerctl next'
alias mprev='playerctl previous'

alias xclipb='xclip -selection clipboard'
alias vimtemp='vim $(mktemp)'
alias vimtempjs='vim $(mktemp --suffix=".js")'

alias pomo='gnome-pomodoro'
alias sitdown='shutdown'

alias dunst-pause='killall -SIGUSR1 dunst'
alias dunst-resume='killall -SIGUSR2 dunst'
