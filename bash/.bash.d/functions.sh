fssh() { ssh $@ -t fish; }
af() { firefox-work -new-tab https://affinitylive.jira.com/browse/affinity-$@; }
pr() { firefox-work -new-tab https://bitbucket.org/affinitylive/affinitylive/pull-requests/$@; }
twdep() { task $@ status.not:deleted export | twdeps --format svg | bcat; }
ts() { clear; task $@ limit:5; }
jsonprint() { python -m json.tool < $1 | less; }
someday() { echo "$@" >> ~/Documents/personal-management/Todo/someday.txt; }
addtodoc() { echo "$@" >> ~/Dropbox/Personal\ management/@Drafts/2018-10-31\ docs\ at\ +accelo.md; }
vimd() { pushd "$1" >/dev/null; vim .; popd >/dev/null; }
tp() { for pr in $@; do tw +pr pro:code-review Review $pr; done; }
twait() { t $1 stop; t $1 mod wait:2h; ts; }
tnext() { t $1 done; t $2 start; pr $3; }
help-update-staging() { pushd ~/Accelo/affinitylive && git fetch && al-update-staging $(curl https://gist.githubusercontent.com/victorzchan/b5a3574e49ead08ca7e6063b6672d777/raw/7982cf3dcc7258fa6f8cd7b0421d3d599e4b7e29/Stagings| fzf) $(git branch -r 2>/dev/null | fzf) && popd; }
htmlize() { TEMPFILE=`mktemp --suffix=.html` && pandoc -o $TEMPFILE $1 && google-chrome $TEMPFILE; }
