const { json } = require("mrm-core");

module.exports = task;

function task() {
	json(".eslintrc.json")
		.merge({
			parserOptions: { ecmaVersion: 6 },
			env: { node: true },
		})
		.save();
}
