const { json } = require("mrm-core");

module.exports = task;

function task() {
	json(".eslintrc.json")
		.merge({ env: { browser: true } })
		.save();
}
