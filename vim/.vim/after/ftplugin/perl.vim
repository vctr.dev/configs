setlocal formatprg=perltidy
setlocal keywordprg=perldoc
compiler perlcritic
nnoremap <buffer> <leader>b :w !perl<cr>
vnoremap <buffer> <leader>b :w !perl<cr>
