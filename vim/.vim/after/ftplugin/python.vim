setlocal keywordprg=pydoc3
nnoremap <buffer> <leader>b :w !python3<cr>
vnoremap <buffer> <leader>b :w !python3<cr>
let g:pymode_python = 'python3'
