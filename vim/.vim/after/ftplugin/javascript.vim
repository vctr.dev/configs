setlocal formatprg=prettier-eslint\ --stdin\ --parser\ babel
nnoremap <buffer> <leader>b :w !node<cr>
vnoremap <buffer> <leader>b :w !node<cr>
