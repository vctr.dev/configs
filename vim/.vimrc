set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'flazz/vim-colorschemes'
Plugin 'bling/vim-airline'
Plugin 'victorzchan/vim-es6'
Plugin 'SirVer/ultisnips'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-fugitive'
Plugin 'ervandew/supertab'
Plugin 'ycm-core/YouCompleteMe'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'w0rp/ale'
Plugin 'mattn/emmet-vim'
Plugin 'leafgarland/typescript-vim'
Plugin 'nathanaelkane/vim-indent-guides'

" Snipmate Dependencies
" Plugin 'MarcWeber/vim-addon-mw-utils'
" Plugin 'tomtom/tlib_vim'
" Plugin 'garbas/vim-snipmate'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

let g:markdown_folding = 1

let g:netrw_banner=0
let g:netrw_preview=1     " preview open: left
let g:netrw_winsize=10    " % width when open split
let g:netrw_liststyle = 3 " default list style: tree

" 'Plugin SirVer/ultisnips' Settings
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_enable_diagnostic_signs = 0
let g:SuperTabDefaultCompletionType = "context"

" 'Plugin vim-airline' Settings
let g:airline#extensions#branch#format = 2
let g:airline#extensions#branch#use_vcscommand = 1
let g:airline#extensions#ale#enabled=1

if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif
let g:airline_symbols.maxlinenr = ''

" 'Plugin junegunn/fzf.vim' Settings
let g:fzf_layout = { 'down': '~10%' }

" 'Plugin w0rp/ale' Settings
let g:ale_fixers = {
\   'html': ['prettier'],
\   'json': ['prettier'],
\   'python': ['autopep8'],
\   'perl': ['perltidy'],
\   'javascript': ['prettier-eslint'],
\   'typescript': ['tslint'],
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\}
let g:ale_linters = {
\ 'javascript': ['eslint'],
\ 'typescript': ['tsserver', 'tslint'],
\ 'xml': ['xmllint'],
\ 'markdown': ['markdownlint'],
\ 'python': ['pylint3']
\ }
let g:ale_echo_cursor=0
let g:ale_lint_on_text_changed=0
let g:ale_lint_on_insert_leave=0

" Plugin 'nathanaelkane/vim-indent-guides' Settings

let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=black
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=237

" Mappings
vnoremap <Leader>gl :Gllog<CR>
vnoremap <Leader>gb :Gblame<CR>
nnoremap <Leader>gl :0Gllog<CR>
nnoremap <Leader>gb :Gblame<CR>
vnoremap <Leader>j :!python -m json.tool<CR>
nnoremap <Leader>r :BTags<CR>
nnoremap <Leader>R :Tags<CR>
nnoremap <Leader>F :Ag 
nnoremap <Leader>f :BLines<CR>
nnoremap <Leader>nt :Lexplore<CR>
nnoremap <Leader>nf :Lexplore %:p:h<CR>
nnoremap <Leader>yf :let @+ = expand("%")<CR>
nnoremap <C-p> :GFiles --cached --others --exclude-standard<CR>

command! Myterm terminal ++rows=20 ++close
command! Reload source ~/.vimrc
command! Task term ++rows=20 ++close tasksh
command! -nargs=1 -complete=dir Tabe tabe +lcd\ <args> <args>

" Auto Commands

" Close loclist when buffer is closed
augroup CloseLoclistWindowGroup
  autocmd!
  autocmd QuitPre * if empty(&buftype) | lclose | endif
augroup END

" Settings

set autoindent
set smarttab
set autowrite
set cursorline
set ignorecase
set incsearch
set linebreak
set listchars=tab:>~,nbsp:_,trail:.,eol:$,extends:⇢
set noeol
set nofoldenable
set nohlsearch
set nowrap
set noexpandtab
set shiftwidth=4
set smartcase
set tabstop=4
set sidescroll=5
set splitbelow
set splitright
set exrc                                                 " allow project specific rc
set secure                                               " recommended with exrc
set ttyfast " fast terminal connection
set showcmd " Show incomplete command in bottom right corner
set wildmenu
set wildmode=longest:full,full
set signcolumn=no
set nomodeline

silent! colorscheme dracula
set scrolloff=5
set mouse=a
